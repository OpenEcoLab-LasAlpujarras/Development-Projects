# README

## Introduction

This is the repository of all the projects we are working on.
Every project has its own folder.

## Opening a new project

To ensure that we keep a common documentation structure, openings of project folder are handled moderated under following points.

### filestructure

#### mark down .md

markdown files a form of documentation, which allows further export to pdf

##### examples

Heading 	
# H1
## H2
### H3
Bold 	**bold text**
Italic 	*italicized text*
Blockquote 	> blockquote
Ordered List 	
1. First item
2. Second item
3. Third item
Unordered List 	- First item
- Second item
- Third item

[the markdown guide](https://www.markdownguide.org/)
[cheat sheet](https://www.markdownguide.org/cheat-sheet/)

#### project ReadMe.md

examples

Link 	[title](https://www.example.com)
Image 	![alt text](image.jpg)

this file includes all summirized information and links to further documents

## project overview

