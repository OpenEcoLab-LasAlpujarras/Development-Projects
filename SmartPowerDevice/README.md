## Digital Power Controll

### AC Triac

In our household, most of the appliances are powered from the AC supply such as Lights, TVs, and Fans, etc. We can turn ON/OFF them digitally if needed, using Arduino and Relays by building a Home automation setup. But what if we need to control the power of those devices for example to dim the AC Lamp or to Control the speed of the Fan. In that case, we have to use phase control technique and static switches like TRIAC to control the phase of AC supply voltage.

Here a TRIAC is used to switch the AC lamp, as this is a Power electronic fast switching device which is the best suited for these applications.

Components Used:

    Arduino UNO-1
    MCT2E optocoupler -1
    MOC3021 optocoupler -1
    BT136 TRIAC-1
    (12-0)V, 500mA Step down transformer-1
    1K,10K, 330ohm Resistors
    10K Potentiometer
    AC Holder with Lamp
    AC wires
    Jumpers



https://circuitdigest.com/microcontroller-projects/arduino-ac-light-dimmer-using-triac