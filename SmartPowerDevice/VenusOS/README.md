# Venus OS

## Introduction

https://www.victronenergy.com/live/venus-os:start


## source 

https://github.com/victronenergy/venus
https://updates.victronenergy.com/feeds/venus/release/images/raspberrypi2/?C=M;O=D

https://www.victronenergy.com/media/pg/CCGX/en/configuration.html#UUID-3d1bea6f-30a0-7d84-8ba6-dab25033ba16

## Components

### microcontroller

Raspberry Pi 3 B: supported, including on-board WiFi, and Bluetooth (use v2.70 or newer for bluetooth)

### storage

sd card

### communication

WiFi

ethernet


## Getting started:

    Download the latest image, a .wic.gz file from here, and for the raspberrypi4, go up one folder and select the raspberrypi4 subfolder.
    Get an sdcard, put it in your computer.
    Burn the image file (.wic.gz) on an sdcard. On windows, use win32diskimager or balenaEtcher.
    Insert the sdcard in the raspberry pi.
    Connect to your local router, using an ethernet cable.
    Power it on.
    In case you have a display connected to the HDMI output port, you'll see the Root console: a terminal prompt where you're logged in as root; see below in case you want to see the GUI on the hdmi output.
    (optional) setup Wifi: using the VictronConnect App, you're able to join the rpi into a WiFi network
    Access the UI, aka Remote Console, which requires first to find the IP address.

    Use VictronConnect App; it will scan you network, and, assuming your phone is in the same lan/wifi network as the Pi, it will find your device.
    Use VictronConnect App; scan via Bluetooth; to see IP settings for both LAN and WiFi.
    Type http://venus.local/ in your browser.
    In case you do have a display and see the terminal prompt, type ifconfig to see its ip configuration.

    Open a browser, and navigate to the ip address, for example http://192.168.52.3. If all went well, you will see the Remote Console page loading, and will be able to browse the menus.
    The VRM Portal id is derived from the Ethernet LAN MAC address, stripped from its colons and in lower case. When in doubt, login to Remote Console, then Settings -> VRM Portal. The ID is shown in that menu.

To later update the now installed Venus OS, see last chapter of this document.


## operatingsysten Rasbian

Root Access

[How to Login as Root on Raspberry Pi OS?](https://raspberrytips.com/login-as-root/)

[Enabling SSH Access for Root Login in Raspberry Pi with Raspbian OS](http://www.vidisonic.com/enabling-ssh-access-root-login-in-raspberry-pi-with-raspbian-os/)


