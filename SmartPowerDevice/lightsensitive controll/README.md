## lightsensitive controll

### hardware


### software (arduino code)

// arduino-light-sensor-triggers-relay incl delay counter 

// constants won't change
const int LIGHT_SENSOR_PIN = A0; // Arduino pin connected to light sensor's  pin
const int RELAY_PIN        = 3;  // Arduino pin connected to Relay's pin
const int ANALOG_THRESHOLD = 450;  

// variables will change:
int analogValue;

void setup() {
  pinMode(RELAY_PIN, OUTPUT); // set arduino pin to output mode
    Serial.begin(9600);

}

void loop() {
  analogValue = analogRead(LIGHT_SENSOR_PIN); // read the input on analog pin
    float voltage = analogValue * (5.0 / 1023.0);

       Serial.println(analogValue);

  int var = 0; 
  while (var < 5 && analogValue < ANALOG_THRESHOLD) { 
               delay(1000); 
               var++; }
               
  if(var=5 && analogValue < ANALOG_THRESHOLD) { 
                   digitalWrite(RELAY_PIN, LOW); } // turn off Relay 

    else {
    digitalWrite(RELAY_PIN, HIGH); 
    var = 0;
           }
       }

