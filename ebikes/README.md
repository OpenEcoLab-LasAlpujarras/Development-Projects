# open ebike building projects

# Table of contents
1. [Introduction](##introduction)
2. [components](##components)
    1. [Sub paragraph](#subparagraph1)
3. [Another paragraph](#paragraph2)


## introduction

this repository documentents the progress to a open hardware ebike.
this project include the research on the ebike part market to see what technology is most common and which ne invention are relevant


## components


### kits

(Bafang 36V 250W Ebike Motor Wheel Electric Bike Conversion Kit 20/26/27.5/700C inch Rear Hub Engine with 22.5Ah Bicycle Battery)[https://www.aliexpress.com/item/32908957618.html?spm=a2g0o.productlist.0.0.124d29abXwvWZC&algo_pvid=629160d2-6c5a-4921-8f98-b1ef6bd79b3a&algo_exp_id=629160d2-6c5a-4921-8f98-b1ef6bd79b3a-44&pdp_ext_f=%7B%22sku_id%22%3A%2212000027049288761%22%7D&pdp_npi=2%40dis%21EUR%21%21406.42%21%21%21%21%21%40210318b916528016675078749e86b6%2112000027049288761%21sea]

<img src="Basic Block Diagram of Pedelec Model E-bike.jpg" alt="alt text" title="image Title" />

Basic Block Diagram of Pedelec Model E-bike

<img src="672aa77814electric-bicycle-electric-motor.jpg" alt="alt text" title="image Title" />

electric-bicycle-electric-motor

### motor


<img src="motor_parameter.png" alt="alt text" title="image Title" />

 

### hubmotors

https://www.electricbike.com/hubmotors/


#### Motor Specification (example):

Motor Model: BBS01B
Bafang Model: MM G340.250
Power:250W
Voltage:36V
Controller Current: 15A
No Load Speed(RPM) :83-87
Load Speed(RPM):78-83

MAX Torque:80 N.m

Top Speed:28km/h

Efficiency: > 80%

PAS SENSOR : Speed Sensor

Reduction Ratio: 1 : 21.9

Certification: CE/EN 14764/ROHS

Waterproof Grade:IP65

Bracket bottom length:68mm

Operation Temperature:-20-45℃

Waterproof Grade: IP65

Display Working Temperature: -20-80C


#### gear

#### power transmission

https://www.elecycles.com/blog/post/how-much-do-you-know-about-the-e-bike-motor-parameters/

### battery

### charger

### controller

(E-bike Charger Reference Design)[https://www.monolithicpower.com/reference-design-electrical-bike]

<img src="controller board.png" alt="alt text" title="image Title" />

controller board

<img src="E-bike block diagram.jpg" alt="alt text" title="image Title" />

### display

<img src="display_options.png" alt="alt text" title="image Title" />


### accelerator throttle

### breaks - break sensor

###


