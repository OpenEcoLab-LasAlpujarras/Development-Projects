
**Installing-MQTT-BrokerMosquitto-on-Raspberry-Pi**

https://www.instructables.com/Installing-MQTT-BrokerMosquitto-on-Raspberry-Pi/

DIY BMS v2.0 - Einfach, günstig, wireless, erweiterbar & komplett erklärt

https://www.youtube.com/watch?v=kVkvUrwA6FU

So wie versprochen alle Daten zum Download, in dem Zip sind folgende Dateien:

    BMSNode.ino: Arduino Programm für die Spannungs/Temperaturmessung mit dem D1 Mini

    Stromsensor.ino: Arduino Programm für die Kontaktlose Strommessung mit dem D1 Mini und einem Hall Sensor

    Broker-Raspberry.py: MQTT Broker für den Rapsberry Pi, nimmt die Daten von den Sensoren an und schickt diese an einer InfluxDB

    Schaltplan_qucs.sch: Schaltplan des Spannungs/Temperatursensors mit dem D1Mini (zum Anzeigen Qucs verwenden)

     Ansonsten müsst ihr eine InfluxDB und Grafana am laufen haben. Hierfür gibts sehr viele gute Anleitungen im Internet. Einfach mal Grafana InfluxDB Raspberry Pi in Google eingeben.

    https://forum.drbacke.de/viewtopic.php?t=42

    Schaltplan.eps: Der Schaltplan als Grafik

Ansonsten müsst ihr eine InfluxDB und Grafana am laufen haben. Hierfür gibts sehr viele gute Anleitungen im Internet. Einfach mal Grafana InfluxDB Raspberry Pi in Google eingeben.

