## Questions for the application

1. Brief description: Summarize the problem area, the most important goals of the project and your solution. (200 words)

2. Which social problem do you want to solve with the project? (150 words)

3. Who is the target group and how should your open hardware solution reach them? How do you plan to involve the target group in the development? (100 words)

4. What specific added value do you see in making your project available as Open Hardware? (150 words)

5. What skills do you bring with you to implement the project? (100 words)

6. How do you want to technically implement your project? What equipment and materials are required? (200 words)

7. Which Open Workshop can you use? Have you already made contact? Or can you do without? (100 words)

8. What similar solutions already exist that you can connect to? How does your project differ from others? (100 words)

9. Have you already worked on the idea? If so, briefly describe the current status and explain the new features that are to be added as part of the fund/funding period. (100 words)

10. Briefly outline the most important milestones with an estimate of the workload in hours that you want to implement in the funding period of max. 6 months. (100 words)

11. What is your long-term goal? What happens after the funding period? (100 words)

12. What is missing? Write here what you wanted to say, but what there was no space for. (100 words)
Optional


## Fragestellung fuer die Bewerbung

1. Kurzbeschreibung: Fasst das Problemfeld, die wichtigsten Ziele des Vorhabens, sowie eure Lösung zusammen. (200 Worte)

2. Welches gesellschaftliche Problem wollt ihr mit dem Projekt lösen? (150 Worte)

3. Wer ist die Zielgruppe und wie soll eure Open Hardware Lösung sie erreichen? Wie plant ihr die Zielgruppe in die Entwicklung einzubinden? (100 Worte)

4. Welchen konkreten Mehrwert seht ihr darin, euer Projekt als Open Hardware zur Verfügung zu stellen? (150 Worte)

5. Welche Kompetenzen bringt ihr mit, um das Projekt umzusetzen? (100 Worte)

6. Wie wollt ihr euer Projekt technisch umsetzen? Welche Geräte und Materialien werden benötigt? (200 Worte)

7. Welche Offene Werkstatt könnt ihr nutzen? Habt ihr schon Kontakt aufgenommen? Oder kommt ihr ohne aus? (100 Worte)

8. Welche ähnlichen Lösungen gibt es schon, an die ihr anschließen könnt? Worin grenzt sich euer Projekt von anderen ab? (100 Worte)

9. Habt ihr schon an der Idee gearbeitet? Wenn ja, beschreibt kurz den aktuellen Stand und erklärt die Neuerung, die im Rahmen des Funds/Förderzeitraums hinzukommen sollen. (100 Worte)

10. Skizziert kurz die wichtigsten Meilensteine mit Schätzung des Arbeitsaufwands in Stunden, die ihr im Förderzeitraum von max. 6 Monaten umsetzen wollt. (100 Worte)

11. Was ist euer langfristiges Ziel? Wie geht es nach der Förderdauer weiter? (100 Worte)

12. Was fehlt? Schreibt hier was ihr noch sagen wolltet, aber wozu es kein Feld gab. (100 Worte)
Optional